const TelegramBot = require('node-telegram-bot-api')

// TOKEN="your_token" node index.js
const token = process.env.TELEGRAM_TOKEN || 'YOUR_TELEGRAM_BOT_TOKEN'
const bot = new TelegramBot(token, { polling: true })

const showSummary = require('./utils/showSummary.js')
const getRandomArticle = require('./utils/getRandomArticle.js')

bot.onText(/\/summary (.+)/, async (msg, match) => {
  showSummary(bot, msg.chat.id, match[1])
})

bot.onText(/\/suggest/, async (msg) => {
  const chatId = msg.chat.id
  const articles = await getRandomArticle()
  articles.forEach(article => {
    showSummary(bot, chatId, article.title)
  })
})

bot.onText(/\/subscribe/, async (msg, match) => {
  const chatId = msg.chat.id
  bot.sendMessage(chatId, `Subscribe you into the Inukathon bot ${chatId}`)
})

bot.on('callback_query', async function onCallbackQuery (callbackQuery) {
  const [ action, title ] = callbackQuery.data.split(/\s(.*)/s)
  const msg = callbackQuery.message
  const opts = {
    chat_id: msg.chat.id,
    message_id: msg.message_id
  }
  let text

  if (action === 'good') {
    text = `Thank you for your feedback (${msg.message_id}:${title})`
    bot.sendMessage(msg.chat.id, text)
  } else if (action === 'bad') {
    text = `Thank you, we will recommend you a better article. (${msg.message_id}:${title})`
    bot.sendMessage(msg.chat.id, text)
  } 
  
  // else if (action === 'random') {
  //   bot.sendMessage(msg.chat.id, '/random')
  //   const title = await getRandomArticle()
  //   showSummary(bot, msg.chat.id, title)
  // }

  bot.editMessageReplyMarkup(null, opts)
})

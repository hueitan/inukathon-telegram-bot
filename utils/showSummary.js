module.exports = async (bot, chatId, title) => {
  const response = await fetch(`https://en.wikipedia.org/api/rest_v1/page/summary/${title}`)
  const summary = await response.json()
  let extractHtml = summary.extract_html.slice(3, -4)
  extractHtml = extractHtml.replace(/<br(\/)?>/ig, '')
  extractHtml = extractHtml.replace(/<span.*?>.*?<\/span>/ig, '')
  const reply_markup = {
    remove_keyboard: true,
    inline_keyboard: [
      [
        {
          text: '👍',
          callback_data: `good ${title}`
        },
        {
          text: '👎',
          callback_data: `bad ${title}`
        },
        // {
        //   text: '🎲',
        //   callback_data: 'random'
        // },
        {
          text: '🌐',
          url: summary.content_urls.desktop.page
        }
      ]
    ]
  }
  if (summary && summary.thumbnail) {
    bot.sendPhoto(chatId, summary.thumbnail.source, {
      caption: extractHtml,
      parse_mode: 'HTML',
      reply_markup

    })
  } else {
    // bot.sendMessage(chatId, extractHtml, {
    //   parse_mode: 'HTML',
    //   reply_markup
    // })
  }
}

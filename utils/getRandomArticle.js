module.exports = async function () {
  const random = await fetch('https://en.wikipedia.org/w/api.php?format=json&action=query&generator=random&grnnamespace=0&prop=revisions%7Cimages&rvprop=content&grnlimit=3')
  const object = await random.json()
  const pages = object.query.pages

  return Object.values(pages)
}
